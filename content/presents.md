---
title: Presents
include_footer: true
---

{{% title3 "Books and Games" %}}
Sophia enjoys items around boardgames, books, and computer games.  

Of course any gift is welcome!

However, purchasing gifts can be tricky, and she does already have a lot of the above.  
So if you are not sure, Sophia has agreed to keep it simple with gift cards from bookstores, boardgame stores, or games stores.

Some ideas:
- Bookstores
  - Book Face
  - or other small Australian based companies to support are always great!
- Boardgames
  - Good Games
  - or other Canberra based games shops.
- Games:
  - Nintendo Switch
  - Xbox
  - EBGames/Zing
  - JB Hifi

We generally like to support smaller Canberra business and experiences, so any ideas with that are very welcome.