---
title: About
include_footer: true
---

{{% title3 "§1 The Family" %}}
This website is to organise events that Elric, Sophia, Dominic and Kat(ESDK) are hosting. 
<br>

{{% title3 "§2 Contact" %}}
{{% subtitle5 "§2.1 Kat" %}}
Kat can be contacted on <a href="tel:0455831272">0455 831 272</a> or via e-mail at kat@esdk.casa

<br>

{{% subtitle5 "§2.2 Elric" %}}
Elric can be contacted on <a href="tel:0413 224 841">0413 224 841</a> or via e-mail at elric@esdk.casa